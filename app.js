
// This API registers the service worker for the app
navigator.serviceWorker.register("sw.js")
.then(
    function(registration) {
        
    }
).catch(
    function(reason) {
        throw new Error("Failed to register service worker");
    }
);

var installEvent;

// Need to wait to be given permission to install the app before allowing the user to install it
window.addEventListener("beforeinstallprompt", event => {
    installEvent = event;
});

function install() {
    if (installEvent) {
        installEvent.prompt();
    }
}