/*
 * A rather tinkersome, basic Service Worker example  
 *
 * You can find helpful explanations of the code here:
 * https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API/Using_Service_Workers
 */

// self is always the global option
// Fetch events are triggered whenever a resource in the 'scope' of your PWA is requested
self.addEventListener('fetch', event => {
	// Here we grab hold of the request so we can respond as we like
    event.respondWith(
    	// Respond with a matching resource from our cache. This returns a promise, which
    	// we can attach a .then() callback to, to do things with when the promise completes
        caches.match(event.request)
            .then(function(response) {
                // Even if we have a cached version of the resource, we fetch
                // and update the cache
                var fetchPromise = fetch(event.request).then(
                    function(response) {
                        caches.open("firstpwa").then(function(cache) {
                        	// Clone the resource from the response and stick it in the cache
                            cache.put(event.request, response.clone());
                            return response;
                        });
                    });
                // We use the currently cached version if it's there
                return response || fetchPromise;
            })
        );
    }); 

// This notes all the resources we want to cache when the user installs our PWA, so it
// can be used when there is no network connectivity.
var urls = ['/', 'public/stylesheets/style.css', 'app.js', 'index.html'];
self.addEventListener('install', event => {
    event.waitUntil(
        caches.open("firstpwa").then(
            function(cache) {
                return cache.addAll(urls);
            }
        )
    )
});