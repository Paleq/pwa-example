# PWA example

A very basic PWA example for demonstration purposes, based on the presentation at Devoxx 2019 (London).

To run: 
- install the latest version of Node.js
- clone the repo
- run "npx serve" from within the cloned folder
- navigate to http://localhost:5000